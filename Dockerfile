FROM registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

LABEL "EAAS_EMULATOR_TYPE"="visualboyadvance"
LABEL "EAAS_EMULATOR_VERSION"="git+eaas-01032019"

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
visualboyadvance

ADD metadata /metadata